# f = open("input_Q1a.txt", "r")
def to_num(file):
    result =  { 63: 0, 6: 1, 91: 2, 79: 3, 102: 4, 109: 5, 95: 6, 7: 7, 127: 8, 111: 9, 0: ' ' }
    f = open(file, 'r')
    string = []
    binary = [0, 1, 0, 2, 64, 32, 4, 8, 16]
    count = 0
    for j in range(3):
        line = f.readline()
        for i in range(3):
            string.append(line[i])
    for i in range(len(string)):
        if string[i] == "|" or string[i] == "_":
            count += binary[i]
    return result[count]

print(to_num("input_Q1a.txt"))

